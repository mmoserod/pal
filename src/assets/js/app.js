import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

import './lib/extras';

var $order = window.localStorage.getItem("order");
var $pinLimit = 3;
var $pins = JSON.parse(window.localStorage.getItem("pins"));
var $names = [];
var $ids = new Object();

if ($pins == null) {
  $pins = [];
  window.localStorage.setItem("pins", JSON.stringify($pins));
}

if ($order == null) {
  $order == "default";
  window.localStorage.setItem("order", $order);
}

$(document).foundation();

$(function() {
  $(".pinthis").show();
  $(".tack").on("click", pin);

  $.each($(".grid .cell .post a .cardfull .info-cell .info .name"), function(i, $name) {
    var $text = $($name).text();
    $names.push($text);
    $ids[$($name).parent().parent().parent().parent().parent().attr("id").replace("-", "")] = $text;
  });

  $("#sort-toggle").click(function() {
    var $fa = $(this).children(".fa");
    if ($fa.hasClass("fa-sort-alpha-asc")) {
      $("#main-grid").addClass("asc").removeClass("desc");
    } else {
      $("#main-grid").addClass("desc").removeClass("asc");
    }

    $fa.toggleClass("fa-sort-alpha-asc").toggleClass("fa-sort-alpha-desc");
    sortItems();
  });
});

// load pins
$(window).load(function() {
  if ($order != "default") {
    $("#main-grid").addClass($order);
    sortItems();
  }

  setTimeout(function() {
    $.each($pins, function(i, id) {
      var $this = $("#"+id+" .pinthis .tack");
    
      var $elem = $("#"+id).parent();
      var $clone = $elem.clone();
    
      $clone.find(".tack").on("click", pin);
      $clone.find(".tack").addClass("pinned");
    
      $(".grid").prepend($clone)
        .isotope("prepended", $clone)
        .isotope("remove", $elem)
        .isotope("layout");
    });
  }, 450);
});

// pin grid item to the top of the list
function pin() {
  if ($(".pinned").length >= $pinLimit && !$(this).hasClass("pinned")) {
    return false;
  }

  $(this).toggleClass("pinned");

  var $elem = $(this).parent().parent().parent();
  var $clone = $elem.clone();
  var $id = $(this).parent().parent().attr("id");

  $clone.find(".tack").on("click", pin);

  if ($(this).hasClass("pinned")) {
    $(".grid").prepend($clone)
      .isotope("prepended", $clone)
      .isotope("remove", $elem)
      .isotope("layout");
    
    $pins.push($id);
  } else {
    $(".grid").append($clone)
      .isotope("appended", $clone)
      .isotope("remove", $elem)
      .isotope("layout");
    
    var $index = $pins.indexOf($id);
    $pins.splice($index, 1);
  }

  localStorage.setItem("pins", JSON.stringify($pins));
}

function sortItems() {
  var $grid = $("#main-grid");
  var $asc = $names.sort();

  if ($grid.hasClass("asc")) {
    $.each($asc, insertItem);
    window.localStorage.setItem("order", "asc");
  } else if ($grid.hasClass("desc")) {
    $.each($asc.reverse(), insertItem);
    window.localStorage.setItem("order", "desc");
  }
}

function insertItem($i, $val) {
  $.each($ids, function($x, $row) {
    if ($row == $val) {
      var $elem = $("#"+$x.replace("post", "post-")).parent();
      var $clone = $elem.clone();
    
      $clone.find(".tack").on("click", pin);
    
      $(".grid").append($clone)
        .isotope("appended", $clone)
        .isotope("remove", $elem)
        .isotope("layout");
    }
  });
}