



// quick search regex
var qsRegex;

// init Isotope
var $grid = $('.grid').isotope({
  	filter: function() {
    	return qsRegex ? $(this).text().match( qsRegex ) : true;
  	}
});

// layout Isotope after each image loads
$grid.imagesLoaded().progress( function() {
  $grid.isotope('layout');
});  

// use value of search field to filter
var $quicksearch = $('.quicksearch').keyup( debounce( function() {
  qsRegex = new RegExp( $quicksearch.val(), 'gi' );
  $grid.isotope();
}, 200 ) );

// debounce so filtering doesn't happen every millisecond
function debounce( fn, threshold ) {
  var timeout;
  return function debounced() {
    if ( timeout ) {
      clearTimeout( timeout );
    }
    function delayed() {
      fn();
      timeout = null;
    }
    timeout = setTimeout( delayed, threshold || 100 );
  }
}




    $(document).ready(function () {

    	// focus cursor to field on load
        $("#odsearch").focus(); 

        // toggle icon view
        $('.viewt').on('click', function(e) {
          $(this).toggleClass("active"); //you can list several class names 
          $('.grid').toggleClass("iconly large-up-8"); //you can list several class names 
          $('.grid').removeClass("listview large-up-12"); //you can list several class names 
          $grid.isotope('layout');
          e.preventDefault();
        });

        // toggle list view
        $('.viewl').on('click', function(e) {
          $(this).toggleClass("active"); //you can list several class names 
          $('.grid').toggleClass("listview").toggleClass("medium-up-2").toggleClass("large-up-4"); //you can list several class names 
          // $('.grid').css('height','1500px');
          // $('.grid').removeClass("iconly grid-x grid-padding-x grid-margin-x grid-margin-y small-up-1 medium-up-2 large-up-4"); //you can list several class names 
          
          $grid.isotope('layout');
          e.preventDefault();
        });



        // toggle pinn
        // $('.tack').on('click', function(e) {
        //   $(this).toggleClass("pinned"); //you can list several class names 

        //   // stamp or unstamp element
        //   var $stamp = $grid.find('.stamp');
        //   var isStamped = false;

        //   // stamp or unstamp element
        //   if ( isStamped ) {
        //     $grid.isotope( 'unstamp', $stamp );
        //   } else {
        //     $grid.isotope( 'stamp', $stamp );
        //   }
        //   // trigger layout
        //   $grid.isotope('layout');
        //   // set flag
        //   isStamped = !isStamped;


        //   e.preventDefault();
        // });

    });