<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>


<div class="cell">
	<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry'); ?> data-equalizer-watch>

		<div class="pinthis">
			<a class="tack" title="Pin this card">
				<i class="fa fa-thumb-tack" aria-hidden="true"></i>
			</a>
		</div>

		<a href="<?php the_field("page_link"); ?>" target="_blank">

			<div class="grid-x cardfull">
			  <div class="small-3 cell thumb-cell" style="background-color:<?php //the_field('card_color'); ?>">
				  <div class="thumb">
				  	<?php
				  		if ( has_post_thumbnail() ) {
				  			the_post_thumbnail();
				  		}
				  	?>
				  </div>
			  </div>
			  <div class="auto cell info-cell">

				  <div class="info">
				  	<h2 class="name"><?php the_title(); ?></h2>

				  	<div class="content">
				  		<?php the_content( __( 'Continue reading...', 'foundationpress' ) ); ?>
				  	</div>
				  	<div class="hiddentags">
				  		 <?php
				  		$posttags = get_the_tags();
				  		if ($posttags) {
				  		  echo '<ul>';
				  		  foreach($posttags as $tag) {
				  		    echo '<li>' .$tag->name. '</li>'; 
				  		  }
				  		  echo '</ul>';
				  		}
				  		?>
				  	</div>
				  </div>			  
			  </div>
			</div>






		</a>
	</div>
</div>