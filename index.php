<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="main-wrap" role="main">
	<article class="main-content">

		<div class="grid-x grid-padding-x grid-margin-x">
			<div class="small-12 medium-8 cell">
				<input type="text" class="quicksearch" placeholder="Search" id="odsearch" />
			</div>

			<div class="small-4 cell">
				<div id="postheader">
					<div class="views">
						<ul>
							<li><a class="view viewt" data-tooltip aria-haspopup="true" class="has-tip" 
							data-disable-hover="false" tabindex="1" title="Toggle icon view" 
							data-position="left" 
							data-alignment="center"
							data-click-open="false"
							> <i class="fa fa-th-large" aria-hidden="true"></i></a></li>

							<li><a class="view viewl" data-tooltip aria-haspopup="true" class="has-tip" 
							data-disable-hover="false" tabindex="1" title="Toggle list view" 
							data-position="left" 
							data-alignment="center"
							data-click-open="false"
							> <i class="fa fa-list" aria-hidden="true"></i></a></li>
							<li>
								<a href="javascript:void(0);" id="sort-toggle"><i class="fa fa-sort-alpha-asc"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

<div id="main-grid" class="grid grid-x grid-padding-x grid-margin-x grid-margin-y small-up-1 medium-up-2 large-up-4">
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
				<?php endwhile; ?>

				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; // End have_posts() check. ?>

				<?php /* Display navigation to next/previous pages when applicable */ ?>
				<?php
				if ( function_exists( 'foundationpress_pagination' ) ) :
					foundationpress_pagination();
				elseif ( is_paged() ) :
				?>
					<nav id="post-nav">
						<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
						<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
					</nav>
				<?php endif; ?>
		</div>

	</article>
	<?php get_sidebar(); ?>

</div>

<?php get_footer();
